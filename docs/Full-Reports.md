# Полные отчёты

Напишите нам на autocheck.ua@gmail.com для получения API ключа
  
## Заголовки к запросам 
* **API-Key** - Ваш персональный API ключ 
* **Locale** - Желаемый язык локализации. 
Поддерживаемые: `RU`,`UK`,`EN`,`SL`

## Общия информация

Примеры запросов: 
[по Гос. номеру](../reference/CarPlates-Ukraine-Reports.v1.json/paths/~1ua~1summary/post)
, [по Vin коду](../reference/CarPlates-Ukraine-Reports.v1.json/paths/~1vin~1summary/post)

[Структура ответа](../reference/CarPlates-Ukraine-Reports.v1.json/components/schemas/Summary)


### Логика покупки отчета:
1. Загрузка предварительного отчета по гос. номеру или vin коду
```json
/ua/summary {
  "number": "AA9999TC"
}
```
2. Отображение пользователю информации о содержимом отчета
3. Покупка отчета
```json
/ua/summary {
  "number": "AA9999TC",
  "purchase": true
}
```
4. Загрузка дополнительных фотографий
```json
/ua/photos {
  "token": "abcd...yz",
}
```


---

После загрузки предварительного просмотра отчета, обратите внимание на параметр `is_payment_required` если значение `false` то такой отчет нельзя купить, так как у нас нету информации об этом транспортном средстве

Содержимое отчета находится в масиве `unicards`
Подробнее в структуре: [Unicard](../reference/CarPlates-Ukraine-Reports.v1.json/components/schemas/Unicard)

### Действия после покупки отчета:

1. Загрузите дополнительные фотограции используя значение `photo_token` в корне ответа. [Пример запроса и структура ответа](../reference/CarPlates-Ukraine-Reports.v1.json/paths/~1ua~1photo/post)

2. Обратите внимание на параметр `is_refresh_required` в корне ответа, если значение `true` то отчет необходимо будет перезагрузить через `token` полученый при покупке, до тех пор пока значение `is_refresh_required` не будет `false`.
Рекомендуемый интервал 5 минут. Данное действие необходимо когда мы не успели подготовить всю информацию вовремя (Зачастую это информация об аресте на авто)

## Примеры отображения данных

### Заголовок:

![Заголовок](../assets/images/title.png)

```json
{
  "id": "was_on_sale",
  "is_payment_required": false,
  "title": "Было на продаже",
  "subtitle": "Польша",
  "brand": "Volkswagen",
  "model": "Crafter",
  "make_year": 2017,
  "trim": null,
  "price": "65000",
  "price_currency": "zł",
  ...
}
```

### Локация:

![Локация](../assets/images/location.png)

```json
{
  "location": {
        "address": "04128, м. Київ, вул. Туполєва, 19",
        "location": "ТСЦ 8041",
        "label": "Сделана в департаменте",
        "icon": "ic_map",
        "zoom": 15
      }
}
```

### Параметры:

![Параметры](../assets/images/properties.png)

```json
{
  "properties": [
    {
      "icon": "АА",
      "label": "Регион",
      "value": "Город Киев",
      "is_large": true,
      "color": "ffffff"
    },
    {
      "icon": "ic_calendar",
      "label": "Дата первой регистрации",
      "value": "2018-11-09",
      "is_large": true
    },
    ...,
    {
      "icon": null,
      "value": "Последняя запись",
      "is_large": true
    },
    {
      "icon": "ic_calendar",
      "label": "Дата записи",
      "value": "2018-11-09",
      "is_large": false
    },
    {
      "icon": "ic_license_plate",
      "label": "Гос. номер",
      "value": "АА9999ТС",
      "is_large": false
    }
}
```

Параметр с действием: 

![Параметр с действием](../assets/images/property_action.png)

```json
{
  "properties": [
    ...,
    {
      "icon": "ic_files",
      "label": "Полная история США",
      "value": "Отчёт <b>Autocheck</b><br><small>Подробная история автомобиля:<br>• История владельцев,<br>• Проверки одометра,<br>• Проверка повреждений<br>и многое другое</small>",
      "is_large": true,
      "action": "weblink",
      "action_value": "https://api.carplates.app/autocheck/8647f8ce16f2482cb0d10ccdf9e93538"
    }
}
```



### Горизонтальные параметры:

Горизонтальные параметры можно обеденить с обычными параметрами, если нету необходимости экономить место на экране

![Горизонтальные параметры](../assets/images/horizontal_properties.png)

```json
{
  "properties_horizontal_title": "Параметры:",
  "properties_horizontal": [
    {
      "icon": "ic_rgb",
      "label": "Цвет",
      "value": "КРАСНЫЙ"
    },
    ...,
    {
      "icon": "B",
      "label": "Категория/Кузов",
      "value": "УНИВЕРСАЛ",
      "color": "ffffff"
    },
    {
      "icon": "ic_seating",
      "label": "Сидячих мест",
      "value": "6"
    }
  ],
}
```

### Подробная/Дополнительная информация:
 
Если значение `label` отсутствует, тогда `value` становится заголовком. 

Важна очередность отображения данных

![Горизонтальные параметры](../assets/images/details.png)

```json
{
  "details_title": "История регистрации",
  "details": [
    {
      "value": "2018-11-09"
    },
    {
      "label": "Гос. номер",
      "value": "АА9999ТС"
    },
    {
      "label": "Запись",
      "value": "ПЕРВИЧНАЯ РЕГИСТРАЦИЯ ТС ПРИОБРЕТЕННОГО В ТОРГОВОЙ ОРГАНИЗАЦИИ, КОТОРЫЙ ВВЕЗЕН ИЗ-ЗА РУБЕЖА"
    },
    {
      "value": "2018-05-30"
    },
    {
      "label": "Запись",
      "value": "Импортировано в Украину"
    }
  ]
}
```
